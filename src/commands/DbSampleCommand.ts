import { DocTools } from "../tools/DocTools";
import * as vscode from "vscode";
import { Client } from "@notionhq/client";
import { writeFileSync } from "fs";
import { l10n } from "vscode";
import { AbstractCommand } from "./AbstractCommand";
import path = require("path");
import { ConfigDbEditService } from "../service/config/ConfigDbEditService";
import { ConfigDbEdit } from "../models/ConfigDbEdit";
import { DbService } from "../service/DbService";

export class DbSampleCommand extends AbstractCommand {

  private readonly expectedExtCfgFileId = ["json"];
  private cfgService: ConfigDbEditService;

  constructor() {
    super();
    this.cfgService = new ConfigDbEditService();
  }

  run(): void {
    try {
      // Check actual file
      if (DocTools.checkDoc(vscode.window.activeTextEditor, this.expectedExtCfgFileId)) {
        // Get config file fromm actual file
        const dataCfgFile = vscode.window.activeTextEditor?.document?.getText();
        const config: ConfigDbEdit = this.cfgService.loadConfigFile(dataCfgFile);
        this.runDbSample(config);
      }      
    } catch (error) {
      vscode.window.showErrorMessage(`run-${l10n.t("Process error")} : ${error}`);      
    }
  }


/**
 * Get data based on config file 
 */
private async runDbSample(rCfg: ConfigDbEdit) {
  if (rCfg?.notion_key_env) {
    const notionKey = process.env[rCfg.notion_key_env];
    if (notionKey && rCfg?.notion_database_id) {
      // Client connection
      const notion = new Client({ auth: notionKey });
      // Service for handling work
      const dbService = new DbService(notion, rCfg.notion_database_id);
      const data = await dbService.getItems(rCfg.sample_filter, false);
      if (data) {
        if (rCfg.file_sample_items && this.workspaceRootPath) {
          // set path related to workspace folder 
          const fileSample = path.join(this.workspaceRootPath, rCfg.file_sample_items);            
          writeFileSync(fileSample, JSON.stringify(data.results, null, 2));
          const msg = l10n.t("Data written in file");
          vscode.window.showInformationMessage(`${msg} : ${rCfg.file_sample_items}`);  
        }
      }
    } else {
      let msg = l10n.t("Config data missing");
      if (!notionKey) {
        msg += `\n ${l10n.t("Environment variable")} -> ${rCfg.notion_key_env}`;
      }
      // Note that 'notion_database_id' has already been checked by config service 
      vscode.window.showErrorMessage(`${msg} : ${rCfg.file_sample_items}`);  
    }
  }
}
  
}
