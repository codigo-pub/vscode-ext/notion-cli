import { ConfigCalendarService } from "../service/config/ConfigCalendarService";
import { DocTools } from "../tools/DocTools";
import * as vscode from "vscode";
import { ConfigCalendar } from "../models/ConfigCalendar";
import { Client } from "@notionhq/client";
import { CalendarService } from "../service/CalendarService";
import { writeFileSync } from "fs";
import { l10n } from "vscode";
import { AbstractCommand } from "./AbstractCommand";
import path = require("path");

export class CalendarSampleCommand extends AbstractCommand {

  private readonly expectedExtCfgFileId = ["json"];
  private cfgService: ConfigCalendarService;

  constructor() {
    super();
    this.cfgService = new ConfigCalendarService();
  }

  run(): void {
    try {
      // Check actual file
      if (DocTools.checkDoc(vscode.window.activeTextEditor, this.expectedExtCfgFileId)) {
        // Get config file fromm actual file
        const dataCfgFile = vscode.window.activeTextEditor?.document?.getText();
        const config: ConfigCalendar = this.cfgService.loadConfigFile(dataCfgFile);
        this.runCalendarSample(config);
      }      
    } catch (error) {
      vscode.window.showErrorMessage(`run-${l10n.t("Process error")} : ${error}`);      
    }
  }


/**
 * Get data based on config file 
 */
private async runCalendarSample(rCfg: ConfigCalendar) {
  if (rCfg?.notion_key_env) {
    const notionKey = process.env[rCfg.notion_key_env];
    if (notionKey && rCfg?.notion_database_id) {
      // Client connection
      const notion = new Client({ auth: notionKey });
      // Service for handling work
      const autoWeekSimple = new CalendarService(notion, rCfg.notion_database_id);
      const data = await autoWeekSimple.getItems(rCfg.sample_filter);
      if (data) {
        if (rCfg.file_sample_items && this.workspaceRootPath) {
          // set path related to workspace folder 
          const fileSample = path.join(this.workspaceRootPath, rCfg.file_sample_items);            
          writeFileSync(fileSample, JSON.stringify(data.results, null, 2));
          const msg = l10n.t("Data written in file");
          vscode.window.showInformationMessage(`${msg} : ${rCfg.file_sample_items}`);  
        }
      }
    } else {
      let msg = l10n.t("Config data missing");
      if (!notionKey) {
        msg += `\n ${l10n.t("Environment variable")} -> ${rCfg.notion_key_env}`;
      }
      // Note that 'notion_database_id' has already been checked by config service 
      vscode.window.showErrorMessage(`${msg} : ${rCfg.file_sample_items}`);  
    }
  }
}
  
}
