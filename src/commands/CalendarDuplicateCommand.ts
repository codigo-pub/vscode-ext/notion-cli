import { ConfigCalendarService } from "../service/config/ConfigCalendarService";
import { DocTools } from "../tools/DocTools";
import * as vscode from "vscode";
import { ConfigCalendar } from "../models/ConfigCalendar";
import { Client } from "@notionhq/client";
import { CalendarService } from "../service/CalendarService";
import { existsSync, readFileSync, writeFileSync } from "fs";
import { l10n } from "vscode";
import { AbstractCommand } from "./AbstractCommand";
import path = require("path");
import { WeekItem } from "../models/WeekItem";
import { DateTools } from "../tools/DateTools";

export class CalendarDuplicateCommand extends AbstractCommand {
  private readonly expectedExtCfgFileId = ["json"];
  private cfgService: ConfigCalendarService;

  constructor() {
    super();
    this.cfgService = new ConfigCalendarService();
  }

  run(): void {
    try {
      // Check actual file
      if (DocTools.checkDoc(vscode.window.activeTextEditor, this.expectedExtCfgFileId)) {
        // Get config file fromm actual file
        const dataCfgFile = vscode.window.activeTextEditor?.document?.getText();
        const config: ConfigCalendar = this.cfgService.loadConfigFile(dataCfgFile);
        this.runCalendarDuplicate(config);
      }
    } catch (error) {
      vscode.window.showErrorMessage(`run-${l10n.t("Process error")} : ${error}`);
    }
  }

  /**
   * Duplicate data based on config file
   */
  private async runCalendarDuplicate(rCfg: ConfigCalendar) {
    let fileTemplate = "";
    if (rCfg?.notion_key_env && this.workspaceRootPath) {
      const notionKey = process.env[rCfg.notion_key_env];
      if (notionKey && rCfg?.notion_database_id && rCfg?.template_week && rCfg?.start_date && rCfg?.end_date) {
        // Client connexion
        const notion = new Client({ auth: notionKey });
        // Service for handling work to do
        const autoWeekSimple = new CalendarService(notion, rCfg?.notion_database_id);
        // Set template file path related to workspace folder
        fileTemplate = path.join(this.workspaceRootPath, rCfg.template_week);
        // Check if file exists
        if (existsSync(fileTemplate)) {
          // Read template
          const templateWeek = JSON.parse(readFileSync(fileTemplate, "utf8"));
          if (templateWeek && templateWeek?.items.length > 0) {
            let goOn = true;
            let i = 0;
            const startDate = new Date(rCfg.start_date);
            const endDate = new Date(rCfg.end_date);
            // endDate can't be before startDate
            if (endDate.getTime() < startDate.getTime()) {
              vscode.window.showErrorMessage(`${l10n.t("End date is before start date")}. ${l10n.t("Check your config file")} !`);
              return;
            }
            // diff total between start and end
            const timeDifference = endDate.getTime() - startDate.getTime();
            // Prepare progress bar
            await vscode.window.withProgress(
              {
                location: vscode.ProgressLocation.Notification,
                title: `${l10n.t("Process running")}...`,
                cancellable: false,
              },
              async (progress, token) => {
                let percent = 0;
                // Loop to run weeks until end date is reached
                while (goOn && rCfg.start_date) {
                  // Adds the number of days to the base date (starting at 0)
                  const firstDayOfWeek = autoWeekSimple.getRealDateCalculated(new Date(rCfg.start_date), i * 7);
                  // Get date as DD-MM-YYYY for displaying
                  const labelDate = DateTools.getLabelDate(firstDayOfWeek);
                  // Update percentage
                  percent = this.getPercent(timeDifference, firstDayOfWeek, startDate);
                  // update progress bar
                  progress.report({ message: `${labelDate}`, increment: percent });
                  // If the end date is later than firstDayOfWeek, we can go ahead.
                  if (endDate.getTime() > firstDayOfWeek.getTime()) {
                    // Creation of the week!
                    goOn = await autoWeekSimple.createWeek(firstDayOfWeek, endDate, templateWeek.items as WeekItem[]);
                    i++;
                  } else {
                    goOn = false;
                  }
                }
                // Done working...
                vscode.window.showInformationMessage(`${l10n.t("Done")} !`);
              }
            );
          }
        } else {
          vscode.window.showErrorMessage(`${l10n.t("File does not exist")} ! : ${fileTemplate}`);
        }
      } else {
        let msg = l10n.t("Config data missing");
        if (!notionKey) {
          msg += `\n ${l10n.t("Environment variable")} -> ${rCfg.notion_key_env}`;
        }
        // Note that 'notion_database_id' has already been checked by config service
        vscode.window.showErrorMessage(msg);
      }
    }
  }

  /**
   * Returns percentage achievement based on current date and start->end date
   * @param rTotalRefDiff : total diff between start and end
   * @param rCurrentDate : current date
   * @param rStartDate : start date
   * @returns percentage
   */
  private getPercent(rTotalRefDiff: number, rCurrentDate: Date, rStartDate: Date) {
    // Calculate the difference between the current date and the start date in milliseconds
    const currentTimeDifference = rCurrentDate.getTime() - rStartDate.getTime();
    // Calculate percentage, rounded
    const percent = Math.ceil((currentTimeDifference / rTotalRefDiff) * 100);
    // Return percentage
    return percent > 100 ? 100 : percent;
  }
}
