import { DocTools } from "../tools/DocTools";
import * as vscode from "vscode";
import { ConfigDbEdit } from "../models/ConfigDbEdit";
import { Client } from "@notionhq/client";
import { l10n } from "vscode";
import { AbstractCommand } from "./AbstractCommand";
import path = require("path");
import { DbService } from "../service/DbService";
import { ConfigDbEditService } from "../service/config/ConfigDbEditService";
import { DbPageItem } from "../models/DbPageItem";

export class DbEditCommand extends AbstractCommand {
  private readonly expectedExtCfgFileId = ["json"];
  private cfgService: ConfigDbEditService;

  constructor() {
    super();
    this.cfgService = new ConfigDbEditService();
  }

  run(): void {
    try {
      // Check actual file
      if (DocTools.checkDoc(vscode.window.activeTextEditor, this.expectedExtCfgFileId)) {
        // Get config file fromm actual file
        const dataCfgFile = vscode.window.activeTextEditor?.document?.getText();
        const config: ConfigDbEdit = this.cfgService.loadConfigFile(dataCfgFile);
        this.runDbEdit(config);
      }
    } catch (error) {
      vscode.window.showErrorMessage(`run-${l10n.t("Process error")} : ${error}`);
    }
  }

  /**
   * Duplicate data based on config file
   */
  private async runDbEdit(rCfg: ConfigDbEdit) {
    let fileTemplate = "";
    if (rCfg?.notion_key_env && this.workspaceRootPath) {
      const notionKey = process.env[rCfg.notion_key_env];
      if (notionKey && rCfg?.notion_database_id && rCfg?.edit_rules) {
        // Client connexion
        const notion = new Client({ auth: notionKey });
        // Service for handling work to do
        const dbService = new DbService(notion, rCfg?.notion_database_id);
        // Read data with cleanup to take away protected fields (to permit update later)
        const data = await dbService.getItems(rCfg.sample_filter);
        // if data to process
        if (data?.results && data?.results.length > 0) {
          //Process all items to edit data, then we will update them on notion
          const dataEdited = dbService.getEditData(data?.results as DbPageItem[], rCfg.edit_rules);
          if (!dataEdited) {
            vscode.window.showErrorMessage("There was a problem processing the data");
          } else {
            const total = data.results.length;
            let i = 0;
            // Prepare progress bar
            await vscode.window.withProgress(
              {
                location: vscode.ProgressLocation.Notification,
                title: `${l10n.t("Process running")}...`,
                cancellable: false,
              },
              async (progress, token) => {
                let percent = 0;
                let previousPercent = 0;
                // Loop to run data
                for (const item of dataEdited) {
                  i++;
                  // Update percentage
                  percent = this.getPercent(total, i);
                  // update progress bar
                  progress.report({ message: `${i}/${total}`, increment: percent - previousPercent });
                  // store previous percent
                  previousPercent = percent;
                  // actually save data on notion
                  await dbService.saveData(item);
                }
                // Done working...
                vscode.window.showInformationMessage(`${l10n.t("Done")} !`);
              }
            );
          }
        }
      } else {
        let msg = l10n.t("Config data missing");
        if (!notionKey) {
          msg += `\n ${l10n.t("Environment variable")} -> ${rCfg.notion_key_env}`;
        }
        // Note that 'notion_database_id' has already been checked by config service
        vscode.window.showErrorMessage(msg);
      }
    }
  }

  /**
   * Returns percentage achievement
   * @param rTotal : total items
   * @param rCurrent : actual item
   * @returns
   */
  private getPercent(rTotal: number, rCurrent: number) {
    // Calculate percentage, rounded
    const percent = Math.ceil((rCurrent / rTotal) * 100);
    // Return percentage
    return percent > 100 ? 100 : percent;
  }
}
