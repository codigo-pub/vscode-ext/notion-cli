// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { CalendarSampleCommand } from './commands/CalendarSampleCommand';

import path = require('path');
import { CalendarDuplicateCommand } from './commands/CalendarDuplicateCommand';
import { DbSampleCommand } from './commands/DbSampleCommand';
import { DbEditCommand } from './commands/DbEditCommand';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "notion-cli" is now active!');

    // notion-cli.calendar-sample command
    let calendarSample = vscode.commands.registerCommand('notion-cli.calendar-sample', () => {
        let process: ICommand = new CalendarSampleCommand();
        process.run();
    });

    // notion-cli.calendar-duplicate command
    let calendarDuplicate = vscode.commands.registerCommand('notion-cli.calendar-duplicate', () => {
        let process: ICommand = new CalendarDuplicateCommand();
        process.run();
    });

    // notion-cli.db-sample command
    let dbSample = vscode.commands.registerCommand('notion-cli.db-sample', () => {
        let process: ICommand = new DbSampleCommand();
        process.run();
    });

    // notion-cli.db-edit command
    let dbEdit = vscode.commands.registerCommand('notion-cli.db-edit', () => {
        let process: ICommand = new DbEditCommand();
        process.run();
    });

    // Add commands
	context.subscriptions.push(
        calendarSample, 
        calendarDuplicate,
        dbSample,
        dbEdit
    );
}

// This method is called when your extension is deactivated
export function deactivate() {}
