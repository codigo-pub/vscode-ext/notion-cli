/* eslint-disable @typescript-eslint/naming-convention */
import { expect } from "chai";
import * as sinon from "sinon";
import { DbService } from "../../../service/DbService";
import { DbPageItem } from "../../../models/DbPageItem";
import { ConfigDbEdit } from "../../../models/ConfigDbEdit";

suite("DbService Test Suite", () => {
  let dbService: DbService;
  let notionClientMock: any;
  const databaseId = "no-database-id-needed-cause-mock";

  setup(() => {
    const fakeDataRetrieve = getFakeRetrieve();
    // Create a mock for the Client class for testing
    notionClientMock = {
      databases: {
        query: sinon.stub().resolves({ results: fakeDataRetrieve }), // Mock for the query method of notion.databases
      },
      pages: {
        update: sinon.stub().resolves({}), // Mock for the update method of notion.pages
      },
    };
    dbService = new DbService(notionClientMock, databaseId);
  });

  test("should process data correctly", () => {
    const fakeExpected = getFakeExpected();
    const rules = getFakeDbEditConfig().edit_rules;
    expect(rules).exist;
    if (rules) {
      // Call the getEditData method
      const result = dbService.getEditData(fakeExpected as DbPageItem[], rules);
      expect(result).exist;
      expect(result).to.be.an("array").that.has.lengthOf(1);
      expect(result).to.deep.equal(getFakeEditExpected());
    }
  });

  test("should save item correctly", async () => {
    // Call the saveData method event if fake object is not really the noe expected
    const result = await dbService.saveData(getFakeRetrieve()[0]);
    // result should not be undefined
    expect(result).to.be.true;
    // Verify the expected behavior
    // Verify that the pages.update method was called
    sinon.assert.calledOnce(notionClientMock.pages.update);
  });

  test("should retrieve items correctly", async () => {
    // Call the getItems method of the extended class
    const result = await dbService.getItems({}, false);
    // result should not be undefined
    expect(result).exist;
    // Verify the expected behavior
    // Verify that the databases.query method was called
    sinon.assert.calledOnce(notionClientMock.databases.query);
    // Verify that the result of the getItems method is correct
    expect(result).to.deep.equal({ results: getFakeRetrieve() });
  });

  test("should retrieve items correctly after cleanup", async () => {
    // Call the getItems method of the extended class
    const result = await dbService.getItems({}, true);
    // result should not be undefined
    expect(result).exist;
    // Verify the expected behavior
    // Verify that the databases.query method was called
    sinon.assert.calledOnce(notionClientMock.databases.query);
    // Verify that the result of the getItems method is correct
    expect(result).to.deep.equal({ results: getFakeExpected() });
  });

  function getFakeDbEditConfig(): ConfigDbEdit {
    return {
      notion_key_env: "NOTION_CODIGO_KEY",
      notion_database_id: "f4d85afae05241d982cd13b8e9c8fc2c",
      file_sample_items: "./sample_data/sample_dbedit_marco.json",
      sample_filter: {
        and: [
          {
            property: "xxx",
            date: {
              on_or_after: "2024-04-08",
            },
          },
          {
            property: "xxx",
            date: {
              on_or_before: "2024-04-08",
            },
          },
        ],
      },
      edit_rules: [
        {
          propertyKey: "Nom",
          propertyValue: "This is the new text",
          rule: {
            contains: "premiers",
          },
        },
        {
          propertyKey: "Description",
          propertyValue: "Travail sur le sprint",
          rule: {
            eq: "Travail sur la roadmap",
          },
        },
        {
          propertyKey: "E-mail",
          propertyValue: "john.doe@gmail.com",
          rule: {
            eq: null,
          },
        },
        {
          propertyKey: "Téléphone",
          propertyValue: "XX XX XX XX",
          rule: {
            contains: "01 22 33 44 55",
          },
        },
        {
          propertyKey: "Junior",
          propertyValue: false,
          rule: {
            eq: true,
          },
        },
        {
          propertyKey: "lien projet",
          propertyValue: "this link will not be edited",
          rule: {
            neq: "https://www.codigo.best",
          },
        },
        {
          propertyKey: "Trombinoscope",
          propertyValue: "cc155270-9d15-41a5-93b8-b1a6e2af4952",
          rule: {
            eq: "7431aee3-8e8b-48fc-9eee-14fb9583a954",
          },
        },
        {
          propertyKey: "Type",
          propertyValue: {
            id: "training",
            name: "Réunion",
            color: "red",
          },
          rule: {
            eq: {
              id: "training",
              name: "Formation",
              color: "pink",
            },
          },
        },
        {
          propertyKey: "Numero",
          propertyValue: 456,
          rule: {
            eq: 123,
          },
        },
        {
          propertyKey: "Status",
          propertyValue: {
            id: "xxxxxxxxxxxxxxxxx",
            name: "Terminé",
            color: "yellow",
          },
          rule: {
            eq: {
              id: "106e0828-d896-438b-82e9-b4f635804d8d",
              name: "En cours",
              color: "blue",
            },
          },
        },
        {
          propertyKey: "Participants",
          propertyValue: {
            object: "user",
            id: "yyyyyyyyyyyyyyyyyyyyyyyyyyyyy",
            name: "alan smithee",
            avatar_url: "xxxxxxxxxxxxxxx.png",
            type: "person",
            person: {},
          },
          rule: {
            eq: {
              object: "user",
              id: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
              name: "marc flausino",
              avatar_url: "xxxxxxxxxxxxxxx.png",
              type: "person",
              person: {},
            },
          },
        },
        {
          propertyKey: "MultiSel",
          propertyValue:   {
            id: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxx",
            name: "tutu",
            color: "yellow",
          },
          rule: {
            eq: {
              id: "43b0b618-8386-4b0b-83b8-c6b7283dc1be",
              name: "toto",
              color: "pink",
            },
          },
        },        
      ],
    } as ConfigDbEdit;
  }

  /**
   * Create a fake 'results' data as the one retrieved from notion API
   * We use it to see if cleanup has worked
   * The indented fields are the one that wil be deleted
   * Careful, here we return the 'results' value, has an array of items.
   * @returns fake items data retrieved from notion api
   */
  function getFakeRetrieve(): any[] {
    return [
      {
        id: "xxx",
        object: {},
        created_time: "xxx",
        last_edited_time: "xxx",
        created_by: "xxx",
        last_edited_by: "xxx",
        parent: "xxx",
        archived: "xxx",
        in_trash: "xxx",
        url: "xxx",
        public_url: "xxx",
        cover: null,
        icon: {
          type: "external",
          external: {
            url: "https://www.notion.so/icons/groups_blue.svg",
          },
        },
        properties: {
          Nom: {
            id: "title",
            type: "title",
            title: [
              {
                type: "text",
                text: {
                  content: "Les premiers jours",
                  link: null,
                },
                annotations: {
                  bold: false,
                  italic: false,
                  strikethrough: false,
                  underline: false,
                  code: false,
                  color: "default",
                },
                plain_text: "xxx",
                href: null,
              },
            ],
          },
          "E-mail": {
            id: "WfJt",
            type: "email",
            email: null,
          },
          Téléphone: {
            id: "qMn_",
            type: "phone_number",
            phone_number: " 01 22 33 44 55",
          },
          Junior: {
            id: "%7CMGh",
            type: "checkbox",
            checkbox: true,
          },
          "lien projet": {
            id: "%3Fgwi",
            type: "url",
            url: "https://www.codigo.best",
          },
          MultiSel: {
            id: "M%3E%3F%60",
            type: "multi_select",
            multi_select: [
              {
                id: "43b0b618-8386-4b0b-83b8-c6b7283dc1be",
                name: "toto",
                color: "pink",
              },
              {
                id: "68574d11-e1f0-464f-81c1-68eca4449056",
                name: "titi",
                color: "blue",
              },
            ],
          },
          Trombinoscope: {
            id: "OCzA",
            type: "relation",
            relation: [
              {
                id: "e93d16fa-d076-4b7b-aff0-9ea770e8e5ca",
              },
              {
                id: "7431aee3-8e8b-48fc-9eee-14fb9583a954",
              },
            ],
            has_more: false,
          },
          Numero: {
            id: "xxx",
            type: "number",
            number: 123,
          },
          Status: {
            id: "xxx",
            type: "status",
            status: {
              id: "106e0828-d896-438b-82e9-b4f635804d8d",
              name: "En cours",
              color: "blue",
            },
          },
          Description: {
            id: "xxx",
            type: "rich_text",
            rich_text: [
              {
                type: "text",
                text: {
                  content: "Travail sur la roadmap",
                  link: null,
                },
                annotations: {
                  bold: false,
                  italic: false,
                  strikethrough: false,
                  underline: false,
                  code: false,
                  color: "default",
                },
                plain_text: "Travail sur la roadmap",
                href: null,
              },
            ],
          },
          Participants: {
            id: "xxx",
            type: "people",
            people: [
              {
                object: "user",
                id: "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
                name: "john doe",
                avatar_url: "zzzz.png",
                type: "person",
                person: {},
              },
              {
                object: "user",
                id: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                name: "marc flausino",
                avatar_url: "xxxxxxxxxxxxxxx.png",
                type: "person",
                person: {},
              },
            ],
          },
          Hour1: {
            id: "xxx",
            type: "date",
            date: {
              start: "2024-04-08T07:00:00.000+00:00",
              end: "2024-04-08T08:00:00.000+00:00",
              time_zone: null,
            },
          },
          Type: {
            id: "xxx",
            type: "select",
            select: {
              id: "training",
              name: "Formation",
              color: "pink",
            },
          },
          ToDelete1: {
            id: "TjTF",
            type: "rollup",
            rollup: {
              type: "array",
              array: [
                {
                  type: "select",
                  select: {
                    id: "DJ];",
                    name: "🤔 Parenthèse réflexive",
                    color: "orange",
                  },
                },
              ],
              function: "show_original",
            },
          },
          ToDelete2: {
            id: "xxx",
            type: "created_by",
          },
          ToDelete3: {
            id: "xxx",
            type: "created_time",
          },
          ToDelete4: {
            id: "xxx",
            type: "last_edited_by",
          },
          ToDelete5: {
            id: "xxx",
            type: "last_edited_time",
          },
        },
      },
    ];
  }

  /**
   * get fake data as expected from getItems. it means properties may have been deleted
   * @returns expected items data retrieved from getItems
   */
  function getFakeExpected(): any[] {
    return [
      {
        id: "xxx",
        cover: null,
        icon: {
          type: "external",
          external: {
            url: "https://www.notion.so/icons/groups_blue.svg",
          },
        },
        properties: {
          Nom: {
            id: "title",
            type: "title",
            title: [
              {
                type: "text",
                text: {
                  content: "Les premiers jours",
                  link: null,
                },
                annotations: {
                  bold: false,
                  italic: false,
                  strikethrough: false,
                  underline: false,
                  code: false,
                  color: "default",
                },
                plain_text: "xxx",
                href: null,
              },
            ],
          },
          "E-mail": {
            id: "WfJt",
            type: "email",
            email: null,
          },
          Téléphone: {
            id: "qMn_",
            type: "phone_number",
            phone_number: " 01 22 33 44 55",
          },
          Junior: {
            id: "%7CMGh",
            type: "checkbox",
            checkbox: true,
          },
          "lien projet": {
            id: "%3Fgwi",
            type: "url",
            url: "https://www.codigo.best",
          },
          MultiSel: {
            id: "M%3E%3F%60",
            type: "multi_select",
            multi_select: [
              {
                id: "43b0b618-8386-4b0b-83b8-c6b7283dc1be",
                name: "toto",
                color: "pink",
              },
              {
                id: "68574d11-e1f0-464f-81c1-68eca4449056",
                name: "titi",
                color: "blue",
              },
            ],
          },
          Trombinoscope: {
            id: "OCzA",
            type: "relation",
            relation: [
              {
                id: "e93d16fa-d076-4b7b-aff0-9ea770e8e5ca",
              },
              {
                id: "7431aee3-8e8b-48fc-9eee-14fb9583a954",
              },
            ],
            has_more: false,
          },
          Numero: {
            id: "xxx",
            type: "number",
            number: 123,
          },
          Status: {
            id: "xxx",
            type: "status",
            status: {
              id: "106e0828-d896-438b-82e9-b4f635804d8d",
              name: "En cours",
              color: "blue",
            },
          },
          Description: {
            id: "xxx",
            type: "rich_text",
            rich_text: [
              {
                type: "text",
                text: {
                  content: "Travail sur la roadmap",
                  link: null,
                },
                annotations: {
                  bold: false,
                  italic: false,
                  strikethrough: false,
                  underline: false,
                  code: false,
                  color: "default",
                },
                plain_text: "Travail sur la roadmap",
                href: null,
              },
            ],
          },
          Participants: {
            id: "xxx",
            type: "people",
            people: [
              {
                object: "user",
                id: "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
                name: "john doe",
                avatar_url: "zzzz.png",
                type: "person",
                person: {},
              },
              {
                object: "user",
                id: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                name: "marc flausino",
                avatar_url: "xxxxxxxxxxxxxxx.png",
                type: "person",
                person: {},
              },
            ],
          },
          Hour1: {
            id: "xxx",
            type: "date",
            date: {
              start: "2024-04-08T07:00:00.000+00:00",
              end: "2024-04-08T08:00:00.000+00:00",
              time_zone: null,
            },
          },
          Type: {
            id: "xxx",
            type: "select",
            select: {
              id: "training",
              name: "Formation",
              color: "pink",
            },
          },
        },
      },
    ];
  }

  /**
   * get fake data as expected from getEditData.
   * @returns expected items data retrieved from getEditData
   */
  function getFakeEditExpected(): any[] {
    return [
      {
        page_id: "xxx",
        properties: {
          Nom: {
            id: "title",
            type: "title",
            title: [
              {
                type: "text",
                text: {
                  content: "This is the new text",
                  link: null,
                },
                annotations: {
                  bold: false,
                  italic: false,
                  strikethrough: false,
                  underline: false,
                  code: false,
                  color: "default",
                },
                plain_text: "xxx",
                href: null,
              },
            ],
          },
          "E-mail": {
            id: "WfJt",
            type: "email",
            email: "john.doe@gmail.com",
          },
          Téléphone: {
            id: "qMn_",
            type: "phone_number",
            phone_number: "XX XX XX XX",
          },
          Junior: {
            id: "%7CMGh",
            type: "checkbox",
            checkbox: false,
          },
          "lien projet": {
            id: "%3Fgwi",
            type: "url",
            url: "https://www.codigo.best",
          },
          MultiSel: {
            id: "M%3E%3F%60",
            type: "multi_select",
            multi_select: [
              {
                id: "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxx",
                name: "tutu",
                color: "yellow",
              },
              {
                id: "68574d11-e1f0-464f-81c1-68eca4449056",
                name: "titi",
                color: "blue",
              },
            ],
          },
          Trombinoscope: {
            id: "OCzA",
            type: "relation",
            relation: [
              {
                id: "e93d16fa-d076-4b7b-aff0-9ea770e8e5ca",
              },
              {
                id: "cc155270-9d15-41a5-93b8-b1a6e2af4952",
              },
            ],
            has_more: false,
          },
          Numero: {
            id: "xxx",
            type: "number",
            number: 456,
          },
          Status: {
            id: "xxx",
            type: "status",
            status: {
              id: "xxxxxxxxxxxxxxxxx",
              name: "Terminé",
              color: "yellow",
            },
          },
          Description: {
            id: "xxx",
            type: "rich_text",
            rich_text: [
              {
                type: "text",
                text: {
                  content: "Travail sur le sprint",
                  link: null,
                },
                annotations: {
                  bold: false,
                  italic: false,
                  strikethrough: false,
                  underline: false,
                  code: false,
                  color: "default",
                },
                plain_text: "Travail sur la roadmap",
                href: null,
              },
            ],
          },
          Participants: {
            id: "xxx",
            type: "people",
            people: [
              {
                object: "user",
                id: "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",
                name: "john doe",
                avatar_url: "zzzz.png",
                type: "person",
                person: {},
              },
              {
                object: "user",
                id: "yyyyyyyyyyyyyyyyyyyyyyyyyyyyy",
                name: "alan smithee",
                avatar_url: "xxxxxxxxxxxxxxx.png",
                type: "person",
                person: {},
              },
            ],
          },
          Hour1: {
            id: "xxx",
            type: "date",
            date: {
              start: "2024-04-08T07:00:00.000+00:00",
              end: "2024-04-08T08:00:00.000+00:00",
              time_zone: null,
            },
          },
          Type: {
            id: "xxx",
            type: "select",
            select: {
              id: "training",
              name: "Réunion",
              color: "red",
            },
          },
        },
      },
    ];
  }
});
