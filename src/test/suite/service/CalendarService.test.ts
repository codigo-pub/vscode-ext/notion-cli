/* eslint-disable @typescript-eslint/naming-convention */
import { expect } from "chai";
import * as sinon from "sinon";
import { CalendarService } from "../../../service/CalendarService";
import { WeekItem } from "../../../models/WeekItem";

suite("CalendarService Test Suite", () => {
  let calendarService: CalendarService;
  let notionClientMock: any;
  const databaseId = "no-database-id-needed-cause-mock";

  setup(() => {
    const fakeDataRetrieve = getFakeRetrieve();
    // Create a mock for the Client class for testing
    notionClientMock = {
      databases: {
        query: sinon.stub().resolves({ results: fakeDataRetrieve }), // Mock for the query method of notion.databases
        retrieve: sinon.stub().resolves({}), // Mock for the retrieve method of notion.databases
      },
      pages: {
        create: sinon.stub().resolves({}), // Mock for the create method of notion.pages
      },
    };
    calendarService = new CalendarService(notionClientMock, databaseId);
  });

  test("should create a week correctly", async () => {
    const fakeTemplate = getFakeTemplate();
    // Call the createWeek method of the extended class
    const result = await calendarService.createWeek(new Date("2024-04-08"), new Date("2024-04-12"), fakeTemplate.items as WeekItem[]);
    // Verify the expected behavior
    // Verify that the pages.create method was called 2 times (one for day 1 and one for day 3 as days_add config says [1, 3])
    sinon.assert.calledTwice(notionClientMock.pages.create);
    // Verify that the result of the createWeek method is correct, expect true
    expect(result).to.be.true;
  });

  test("should create a week correctly and stop", async () => {
    const fakeTemplate = getFakeTemplate();
    // Call the createWeek method of the extended class
    const result = await calendarService.createWeek(new Date("2024-04-08"), new Date("2024-04-09"), fakeTemplate.items as WeekItem[]);
    // Verify the expected behavior
    // Verify that the pages.create method was called 1 times (one for day 1 but couldn't for day 3 cause out of range)
    sinon.assert.calledOnce(notionClientMock.pages.create);
    // Verify that the result of the createWeek method is correct, expect false
    expect(result).to.be.false;
  });

  test("should retrieve items correctly", async () => {
    // Call the getItems method of the extended class
    const result = await calendarService.getItems({}, false);
    // result should not be undefined
    expect(result).exist;
    // Verify the expected behavior
    // Verify that the databases.query method was called
    sinon.assert.calledOnce(notionClientMock.databases.query);
    // Verify that the result of the getItems method is correct
    expect(result).to.deep.equal({ results: getFakeRetrieve() });
  });

  test("should retrieve items correctly after cleanup", async () => {
    // Call the getItems method of the extended class
    const result = await calendarService.getItems({}, true);
    // result should not be undefined
    expect(result).exist;
    // Verify the expected behavior
    // Verify that the databases.query method was called
    sinon.assert.calledOnce(notionClientMock.databases.query);
    // Verify that the result of the getItems method is correct
    expect(result).to.deep.equal({ results: getFakeExpected() });
  });

  test("should calculate real date correctly", () => {
    // Call the getRealDateCalculated method of the extended class
    const result = calendarService.getRealDateCalculated(new Date(2024, 3, 12), 3);

    // Verify the expected behavior
    // Verify that the result of the getRealDateCalculated method is correct
    expect(result).to.deep.equal(new Date(2024, 3, 15));
  });

  /**
   * Create a fake template data
   * @returns fake template data
   */
  function getFakeTemplate(): any {
    return {
      items: [
        {
          days_add: [1, 3],
          cover: null,
          icon: {
            type: "external",
            external: {
              url: "https://www.notion.so/icons/groups_blue.svg",
            },
          },
          properties: {
            Nom: {
              id: "title",
              type: "title",
              title: [
                {
                  type: "text",
                  text: {
                    content: "First step with notion",
                    link: null,
                  },
                  annotations: {
                    bold: false,
                    italic: false,
                    strikethrough: false,
                    underline: false,
                    code: false,
                    color: "default",
                  },
                  plain_text: "First step",
                  href: null,
                },
              ],
            },
            Participants: {
              id: "notion%3A%2F%2Fmeetings%2Fattendees_property",
              type: "people",
              people: [],
            },
            DateProperty: {
              id: "notion%3A%2F%2Fmeetings%2Fmeeting_time_property",
              type: "date",
              date: {
                start: "2024-04-08T07:00:00.000+00:00",
                end: "2024-04-08T08:00:00.000+00:00",
                time_zone: null,
              },
            },
            Type: {
              id: "notion%3A%2F%2Fmeetings%2Fmeeting_type_property",
              type: "select",
              select: {
                id: "training",
                name: "Formation",
                color: "pink",
              },
            },
          },
        },
      ],
    };
  }

  /**
   * Create a fake 'results' data as the one retrieved from notion API
   * We use it to see if cleanup has worked
   * The indented fields are the one that wil be deleted
   * Careful, here we return the 'results' value, has an array of items.
   * @returns fake items data retrieved from notion api
   */
  function getFakeRetrieve(): any[] {
    return [
      {
        id: "xxx",
        object: {},
        created_time: "xxx",
        last_edited_time: "xxx",
        created_by: "xxx",
        last_edited_by: "xxx",
        parent: "xxx",
        archived: "xxx",
        in_trash: "xxx",
        url: "xxx",
        public_url: "xxx",
        cover: null,
        icon: {
          type: "external",
          external: {
            url: "https://www.notion.so/icons/groups_blue.svg",
          },
        },
        properties: {
          Nom: {
            id: "title",
            type: "title",
            title: [
              {
                type: "text",
                text: {
                  content: "xxx",
                  link: null,
                },
                annotations: {
                  bold: false,
                  italic: false,
                  strikethrough: false,
                  underline: false,
                  code: false,
                  color: "default",
                },
                plain_text: "xxx",
                href: null,
              },
            ],
          },
          Participants: {
            id: "xxx",
            type: "people",
            people: [
              {
                object: "user",
                id: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                name: "marc flausino",
                avatar_url: "xxxxxxxxxxxxxxx.png",
                type: "person",
                person: {},
              },
            ],
          },
          Hour1: {
            id: "xxx",
            type: "date",
            date: {
              start: "2024-04-08T07:00:00.000+00:00",
              end: "2024-04-08T08:00:00.000+00:00",
              time_zone: null,
            },
          },
          Type: {
            id: "xxx",
            type: "select",
            select: {
              id: "training",
              name: "Formation",
              color: "pink",
            },
          },
          ToDelete1: {
            id: "TjTF",
            type: "rollup",
            rollup: {
              type: "array",
              array: [
                {
                  type: "select",
                  select: {
                    id: "DJ];",
                    name: "🤔 Parenthèse réflexive",
                    color: "orange",
                  },
                },
              ],
              function: "show_original",
            },
          },
          ToDelete2: {
            id: "xxx",
            type: "created_by",
          },
          ToDelete3: {
            id: "xxx",
            type: "created_time",
          },
          ToDelete4: {
            id: "xxx",
            type: "last_edited_by",
          },
          ToDelete5: {
            id: "xxx",
            type: "last_edited_time",
          },
        },
      },
    ];
  }

  /**
   * get fake data as expected from getItems. it means properties may have been deleted
   * @returns expected items data retrieved from getItems
   */
  function getFakeExpected(): any[] {
    return [
      {
        cover: null,
        icon: {
          type: "external",
          external: {
            url: "https://www.notion.so/icons/groups_blue.svg",
          },
        },
        properties: {
          Nom: {
            id: "title",
            type: "title",
            title: [
              {
                type: "text",
                text: {
                  content: "xxx",
                  link: null,
                },
                annotations: {
                  bold: false,
                  italic: false,
                  strikethrough: false,
                  underline: false,
                  code: false,
                  color: "default",
                },
                plain_text: "xxx",
                href: null,
              },
            ],
          },
          Participants: {
            id: "xxx",
            type: "people",
            people: [
              {
                object: "user",
                id: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
                name: "marc flausino",
                avatar_url: "xxxxxxxxxxxxxxx.png",
                type: "person",
                person: {},
              },
            ],
          },
          Hour1: {
            id: "xxx",
            type: "date",
            date: {
              start: "2024-04-08T07:00:00.000+00:00",
              end: "2024-04-08T08:00:00.000+00:00",
              time_zone: null,
            },
          },
          Type: {
            id: "xxx",
            type: "select",
            select: {
              id: "training",
              name: "Formation",
              color: "pink",
            },
          },
        },
      },
    ];
  }
});
