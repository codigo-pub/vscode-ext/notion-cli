import { expect } from 'chai';
import * as vscode from 'vscode';
import { ObjectTools } from '../../../tools/ObjectTools';

// import * as myExtension from '../../extension';

suite('ObjectTools Test Suite', () => {
    vscode.window.showInformationMessage('Start ObjectTools tests.');

    setup(() => {});

    test('transformQueryStringIdToUUID should transform querystring id to uuid ', () => {
        expect(ObjectTools.transformQueryStringIdToUUID("28accdc004c94de6b437fd31bf4fd5b2")).to.eq("28accdc0-04c9-4de6-b437-fd31bf4fd5b2");
	});

});
