import { expect } from 'chai';
import * as vscode from 'vscode';
import { DateTools } from '../../../tools/DateTools';

// import * as myExtension from '../../extension';

suite('DateTools Test Suite', () => {
    vscode.window.showInformationMessage('Start DateTools tests.');

    setup(() => {});

    test('getLabelDate test', () => {
        let myDate = new Date("2024-01-15");
        expect(DateTools.getLabelDate(myDate)).to.equal("15-01-2024");
	});

});
