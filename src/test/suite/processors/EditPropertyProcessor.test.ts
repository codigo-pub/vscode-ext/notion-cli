/* eslint-disable @typescript-eslint/naming-convention */
import { expect } from "chai";
import { EditPropertyProcessor } from "../../../processors/edit/EditPropertyProcessor";
import { GetPagePropertyResponse } from "@notionhq/client/build/src/api-endpoints";

// import * as myExtension from '../../extension';

suite("EditPropertyProcessor Test Suite", () => {
  let processor: EditPropertyProcessor;

  setup(() => {
    processor = new EditPropertyProcessor();
  });

  test("process should update title property if rule matches", () => {
    const ruleValue = generateTitleRuleValue();
    const property: GetPagePropertyResponse = {
      type: "title",
      title: [{ text: { content: "Old premiers" } }],
    } as unknown as GetPagePropertyResponse;

    const processedProperty = processor.process(property, ruleValue);
    expect((processedProperty as any).title[0].text.content).to.equal("This is the new text");
  });

  test("process should update rich_text property if rule matches", () => {
    const ruleValue = generateRichTextRuleValue();
    const property: GetPagePropertyResponse = {
      type: "rich_text",
      rich_text: [{ text: { content: "Travail sur la roadmap" } }],
    } as unknown as GetPagePropertyResponse;
    const processedProperty = processor.process(property, ruleValue);
    expect((processedProperty as any).rich_text[0].text.content).to.equal("Travail sur le sprint");
  });

  test("process should update email property if rule matches", () => {
    const ruleValue = generateEmailRuleValue();
    const property: GetPagePropertyResponse = {
      type: "email",
      email: null,
    } as unknown as GetPagePropertyResponse;
    const processedProperty = processor.process(property, ruleValue);
    expect((processedProperty as any).email).to.equal("john.doe@gmail.com");
  });

  function generateTitleRuleValue(): { propertyKey: string; propertyValue: any; rule: any } {
    return {
      propertyKey: "title",
      propertyValue: "This is the new text",
      rule: {
        contains: "premiers",
      },
    };
  }

  function generateRichTextRuleValue(): { propertyKey: string; propertyValue: any; rule: any } {
    return {
      propertyKey: "rich_text",
      propertyValue: "Travail sur le sprint",
      rule: {
        eq: "Travail sur la roadmap",
      },
    };
  }

  function generateEmailRuleValue(): { propertyKey: string; propertyValue: any; rule: any } {
    return {
      propertyKey: "email",
      propertyValue: "john.doe@gmail.com",
      rule: {
        eq: null,
      },
    };
  }
});
