/* eslint-disable @typescript-eslint/naming-convention */
import { CreatePageParameters, GetPagePropertyResponse } from "@notionhq/client/build/src/api-endpoints";
import { WeekItem } from "../models/WeekItem";
import { AbstractNotionService } from "./AbstractNotionService";
import * as vscode from "vscode";
import { l10n } from "vscode";
import { Client } from "@notionhq/client";

export class CalendarService extends AbstractNotionService {

  
  constructor(protected notion: Client, protected databaseId: string) {
    super(notion, databaseId);
    // Add id to the deleted fields cause we don't want it as we are going to create data and not modify it
    this.fields_base_to_delete.push("id");
  }

  /**
   * Week creation
   * @param rStartDate : start date
   * @param rEndDate : end date
   * @param rItems : items to create
   * @returns true if process can be continued, false if process should be stopped 
   */
  async createWeek(rStartDate: Date, rEndDate: Date, rItems: WeekItem[]): Promise<boolean> {
    let total = 0;
    if (rStartDate) {
      // console.log(` ==> Creation of week from date [${this.getLabelDate(rStartDate)}]`);
      for (const item of rItems) {
        if (item.days_add) {
          // Process as many times as there are days_add
          for (const day_add of item.days_add) {
            // Calculates the reference date for data to be created
            const dayRef = this.getRealDateCalculated(rStartDate, day_add);
            // If the date has passed, false is returned
            if (rEndDate.getTime() < dayRef.getTime()) {
              return false;
            }
            // Processes and retrieves the item on the correct day according to day_add
            const myParameter = this.getDataItem(dayRef, item);
            // Add data
            if (myParameter) {
              try {
                await this.notion.pages.create(myParameter);
                total++;                  
              } catch (error) {
                vscode.window.showErrorMessage(`createWeek-${l10n.t("Process error")} : ${error}`);
                // console.error("Oops, insert error ", JSON.stringify(myParameter));
                return false;
              }
            } else {
              return false;
            }
          }
        }
      }
      // console.log("   ==> Items added : %d", total);
      return true;
    } else {
      vscode.window.showErrorMessage(`createWeek-${l10n.t("No date value as parameter, can't create week !")}`);
      return false;
    }
  }

  /**
   * Build item data
   * @param rDayRef : start date to begin the week, it is the reference date
   * @param rItem : item
   * @returns item 
   */
  private getDataItem(rDayRef: Date, rItem: WeekItem): CreatePageParameters | undefined {
    if (rDayRef && rItem) {
      if (rItem.properties) {
        // Browse data properties, extracting key/value from key and myProperty
        for (const [key, myProperty] of Object.entries(rItem.properties)) {
          // For each property, we do what's required, i.e. change the date if necessary.
          this.processDataItemProperty(rDayRef, myProperty as GetPagePropertyResponse);        }
        // Prepare the data to be added to the database
        const parameters: CreatePageParameters = {
          icon: rItem.icon,
          parent: {
            database_id: this.databaseId,
          },
          properties: rItem.properties,
        } as CreatePageParameters;
        // return data
        return parameters;
      } else {
        console.error("getDataItem- no properties retrieved ?!");
      }
    } else {
      console.error("getDataItem- missing parameter !", rDayRef, rItem);
    }
    return undefined;
  }

  /**
   * Builds item data property
   * @param rDayRef : start date to begin the week, it is the reference date
   * @param rItem : item
   * @throws Exception when rPeriod is not correctly valued or rItem is missing
   * @returns
   */
  private processDataItemProperty(rDayRef: Date, rItem: GetPagePropertyResponse): void {
    if (!rItem) {
      throw new Error("processDataItemProperty-Missing item parameter !");
    }
    if (rDayRef) {
      // The data is only processed if it is a 'date' type and if start and end are valued!
      if (rItem?.type === "date" && rItem?.date?.start && rItem?.date?.end) {
        // First convert both dates
        const itemStart = new Date(rItem.date.start);
        const itemEnd = new Date(rItem.date.end);
        // Ensuite affecte les bonnes infos Jour, mois, annee
        itemStart.setFullYear(rDayRef.getFullYear());
        itemStart.setMonth(rDayRef.getMonth());
        itemStart.setDate(rDayRef.getDate());
        itemEnd.setFullYear(rDayRef.getFullYear());
        itemEnd.setMonth(rDayRef.getMonth());
        itemEnd.setDate(rDayRef.getDate());
        // then set values
        // note: we pass the date in UTC but it will be converted by notion into local time
        rItem.date.start = itemStart.toISOString();
        rItem.date.end = itemEnd.toISOString();
      }
    } else {
      const msg = "processDataItemProperty-Processing error, we expect to have a reference date but some information is missing!!!";
      console.error(msg, rDayRef);
      throw new Error(msg);
    }
  }
}
