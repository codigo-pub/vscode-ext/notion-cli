/* eslint-disable @typescript-eslint/naming-convention */
import {
  GetPagePropertyResponse,
  UpdatePageParameters,
} from "@notionhq/client/build/src/api-endpoints";
import { AbstractNotionService } from "./AbstractNotionService";
import * as vscode from "vscode";
import { l10n } from "vscode";
import { DbEditRule } from "../models/DbEditRule";
import { DbPageItem } from "../models/DbPageItem";
import { EditPropertyProcessor } from "../processors/edit/EditPropertyProcessor";
import { Client } from "@notionhq/client";

export class DbService extends AbstractNotionService {

  private processor?: EditPropertyProcessor;

  constructor(protected notion: Client, protected databaseId: string) {
    super(notion, databaseId);
    this.processor = new EditPropertyProcessor();
  }

  /**
   * Save data
   * @param rItem : item to save
   * @returns true if save was done, false otherwise
   */
  async saveData(rItem: UpdatePageParameters): Promise<boolean> {
    // If there is an item, save it on Notion
    if (rItem) {
      try {
        await this.notion.pages.update(rItem);
      } catch (error) {
        vscode.window.showErrorMessage(`saveData-${l10n.t("Process error")} : ${error}`);
        // console.error("Oops, update error ", JSON.stringify(myParameter));
        return false;
      }
      return true;
    } else {
      vscode.window.showErrorMessage(`saveData-${l10n.t("Nothing to do, no item to save")} !`);
      return false;
    }
  }

  /**
   * Edit all data and return UpdatePageParameters array ready to be saved
   * @param rItems : items to edit
   * @param rRules : Rules for edit process
   * @returns true if edit has been done ok
   */
  getEditData(
    rItems: DbPageItem[],
    rRules: DbEditRule[]
  ): UpdatePageParameters[] | undefined {
    let editData:UpdatePageParameters[] = [];
    // If there are things to do (checked also before)
    if (rItems && rRules && rItems.length > 0 && rRules.length > 0) {
      for (const item of rItems) {
        // Process the item
        const myEditedData = this.applyRulesOnItem(rRules, item);
        // Add data
        if (myEditedData) {
          editData.push(myEditedData);
        }
      }
      return editData;
    } else {
      vscode.window.showErrorMessage(`editData-${l10n.t("Nothing to do, check your rules and filter")} !`);
      return undefined;
    }
  }

  /**
   * Apply rules on item, if nothing was done, return undefined
   * @param rRules : rules array
   * @param rItem : item
   * @returns item or undefined if nothing was done
   */
  private applyRulesOnItem(
    rRules: DbEditRule[],
    rItem: DbPageItem
  ): UpdatePageParameters | undefined {
    if (rRules && rItem) {
        if (rItem.properties) {
          // Browse data properties, extracting key/value from key and myProperty
          for (const [key, myProperty] of Object.entries(rItem.properties)) {
            // For each property, we do what's required, i.e. change the date if necessary.
            for (const rule of rRules) {
              //If rule is related to this property, we apply
              if (key === rule.propertyKey) {
                // Process property
                this.processor?.process(myProperty as GetPagePropertyResponse, rule);
              }
            }
          }
          // Prepare the data to be updated to the database
          const parameters: UpdatePageParameters = {
            page_id: rItem.id,
            properties: rItem.properties,
          } as UpdatePageParameters;
          // return data
          return parameters;
        } else {
          console.error("getDataItem- no properties retrieved ?!");
        }
    } else {
      console.error("applyRulesOnItem- missing parameter !", rItem, rRules);
    }
    return undefined;
  }
}
