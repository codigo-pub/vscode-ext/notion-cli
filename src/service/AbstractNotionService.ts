/* eslint-disable @typescript-eslint/naming-convention */
import { Client } from "@notionhq/client";
import { GetDatabaseResponse, QueryDatabaseResponse } from "@notionhq/client/build/src/api-endpoints";
import { l10n } from "vscode";
import * as vscode from "vscode";

export abstract class AbstractNotionService {
  // don't put id on deleted fields here, if you want it deleted, add it in your service like it is done on CalendarService constructor
  protected fields_base_to_delete = [
    "object",
    "created_time",
    "last_edited_time",
    "created_by",
    "last_edited_by",
    "parent",
    "archived",
    "in_trash",
    "url",
    "public_url",
  ];

  // note : 'rollup' data are automatically calculated, so you must not send them.
  protected fields_types_to_delete = ["created_by", "created_time", "last_edited_by", "last_edited_time", "rollup"];

  constructor(protected notion: Client, protected databaseId: string) {}

  /**
   * Retrieve database
   * @returns database
   */
  async getDatabase(): Promise<GetDatabaseResponse> {
    return await this.notion.databases.retrieve({
      database_id: this.databaseId,
    });
  }

  /**
   * Recover a sample data to help create template file
   * @param rSample_filter : data filter
   * @param rCleanup : true to suppress forbidden/parasite fields (created_by, etc.), default to true
   * @returns the answer, to get the items, you have to use the results property!
   */
  async getItems(rSample_filter?: any, rCleanup: boolean = true): Promise<QueryDatabaseResponse | undefined> {
    try {
      // ask for data for this database, using filter
      const queryResponse = await this.notion.databases.query({
        database_id: this.databaseId,
        filter: rSample_filter,
      });
      if (queryResponse?.results.length > 0) {
        // if cleanup asked
        if (rCleanup) {
          this.cleanBaseDataItem(queryResponse);
        }
        return queryResponse;
      } else {
        vscode.window.showWarningMessage(l10n.t("No data retrieved ?! Is database empty ?"));
        return undefined;
      }
    } catch (error) {
      vscode.window.showErrorMessage(`getItems-${l10n.t("Process error")} : ${error}`);
      return undefined;
    }
  }

  
  /**
   * Calculates and returns the real day used to create the data
   * @param rStartDate : starting day of the week
   * @param rDayAdd : days to add
   * @returns
   */
  getRealDateCalculated(rStartDate: Date, rDayAdd?: number): Date {
    // change start and end date based on criterias
    const coeff = 24 * 60 * 60 * 1000;
    return new Date(rStartDate.getTime() + (rDayAdd ?? 0) * coeff);
  }

  /**
   * Remove obsolete or forbidden fields from the base of a data item
   * For example, the 'created_time' field
   * @param rData : data
   */
  protected cleanBaseDataItem(rData: QueryDatabaseResponse) {
    if (rData?.results && this.fields_base_to_delete) {
      // Browse data
      for (const resultData of rData.results) {
        // Deletes basic fields from current data
        for (const fieldDelete of this.fields_base_to_delete) {
          delete (resultData as any)[fieldDelete];
        }
        // Browse properties and clean up too 
        for (const [key, myProperty] of Object.entries((resultData as any).properties)) {
          if (this.canCleanDataProperty(myProperty as any)) {
            delete (resultData as any).properties[key];
          }
        }
      }
    }
  }

  /**
   * Removes obsolete or forbidden fields from a data property
   * Unlike the basic fields, this time you have to search in the "type"!
   * @param rData : data
   * @param rTypeFieldsToDelete : type of fields to be deleted
   */
  protected canCleanDataProperty(rData: any): boolean {
    return (rData && rData["type"] && this.fields_types_to_delete.indexOf(rData["type"]) >= 0);
  }

}
