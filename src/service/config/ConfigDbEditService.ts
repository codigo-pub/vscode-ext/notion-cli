
import { AbstractConfigService } from "./AbstractConfigService";
import { ConfigDbEdit } from "../../models/ConfigDbEdit";


export class ConfigDbEditService extends AbstractConfigService<ConfigDbEdit> {
    checkConfig(rCfg: ConfigDbEdit) {
        this.checkCfgItem("notion_database_id", rCfg.notion_database_id);
        this.checkCfgItem("notion_key_env", rCfg.notion_key_env);
        this.checkCfgItem("file_sample_items", rCfg.file_sample_items);
        this.checkCfgItem("sample_filter", rCfg.sample_filter);
        this.checkCfgItem("edit_rules", rCfg.edit_rules);
    }
}