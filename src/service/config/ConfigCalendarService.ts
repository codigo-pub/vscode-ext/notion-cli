
import { AbstractConfigService } from "./AbstractConfigService";
import { ConfigCalendar } from "../../models/ConfigCalendar";


export class ConfigCalendarService extends AbstractConfigService<ConfigCalendar> {
    checkConfig(rCfg: ConfigCalendar) {
        this.checkCfgItem("notion_database_id", rCfg.notion_database_id);
        this.checkCfgItem("notion_key_env", rCfg.notion_key_env);
        this.checkCfgItem("file_sample_items", rCfg.file_sample_items);
        this.checkCfgItem("sample_filter", rCfg.sample_filter);
        this.checkCfgItem("start_date", rCfg.start_date);
        this.checkCfgItem("end_date", rCfg.start_date);
        this.checkCfgItem("template_week", rCfg.template_week);
    }
}