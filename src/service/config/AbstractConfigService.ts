import { l10n } from "vscode";

export abstract class AbstractConfigService<T> {

  /**
   * Check config data for mandatory fields
   * @throws Exception if a field is not ok
   */
  abstract checkConfig(rCfg: T): void;

  /**
   * Get config file data
   * @param rCfgFileContent : config file content
   * @throws Exception if file can't be processed
   * @returns
   */
  loadConfigFile(rCfgFileContent?: string): T {
    if (!rCfgFileContent) {
      throw new Error("File can't be processed");
    }
    // Parse content and convert to model
    const config = JSON.parse(rCfgFileContent) as T;
    // Check config
    this.checkConfig(config);
    // return config
    return config;
  }

  protected checkCfgItem(rKey: string, rCfgItem: any) {
    if (!rCfgItem) {
      const msgErr = l10n.t("Property is not well defined but it is mandatory ! ");
      ;
      throw new Error(`${msgErr} : [${rKey}]`);
    }
  }
}
