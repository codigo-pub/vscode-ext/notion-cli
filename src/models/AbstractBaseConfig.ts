export abstract class AbstractBaseConfig {
    accessor notion_key_env: string | undefined;
    accessor notion_database_id: string | undefined;
}