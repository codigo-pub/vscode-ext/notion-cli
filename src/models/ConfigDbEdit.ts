import { AbstractBaseConfig } from "./AbstractBaseConfig";
import { DbEditRule } from "./DbEditRule";

export class ConfigDbEdit extends AbstractBaseConfig {
    accessor file_sample_items: string | undefined;
    accessor sample_filter: any | undefined;
    accessor edit_rules: DbEditRule[] | undefined;
}