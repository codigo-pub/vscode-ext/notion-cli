import { CreatePageParameters, UpdatePageParameters } from "@notionhq/client/build/src/api-endpoints";

export class DbPageItem {
    accessor id: string | undefined;
    accessor properties: Record<string, UpdatePageParameters["properties"]> | undefined;
}