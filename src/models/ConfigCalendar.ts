import { AbstractBaseConfig } from "./AbstractBaseConfig";

export class ConfigCalendar extends AbstractBaseConfig {
    accessor template_week: string | undefined;
    accessor file_sample_items: string | undefined;
    accessor sample_filter: any | undefined;
    accessor start_date: string | undefined;
    accessor end_date: string | undefined;
  }