import { CreatePageParameters } from "@notionhq/client/build/src/api-endpoints";

export class WeekItem {
    accessor icon: string | undefined;
    accessor days_add: number[] | undefined;
    accessor properties: Record<string, CreatePageParameters["properties"]> | undefined;
}