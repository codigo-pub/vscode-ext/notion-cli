import { AbstractBaseConfig } from "./AbstractBaseConfig";
import { DbRule } from "./DbRule";

export class DbEditRule {
    accessor propertyKey: string | undefined;
    accessor propertyValue: any | undefined;
    accessor rule: DbRule | undefined;
}