import { AbstractBaseConfig } from "./AbstractBaseConfig";

export class DbRule {
    accessor eq: string | number | any | undefined;
    accessor neq: string | number | any | undefined;
    accessor contains: string | undefined;
}