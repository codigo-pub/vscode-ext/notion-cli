import { GetPagePropertyResponse } from "@notionhq/client/build/src/api-endpoints";
import { IPropertyProcessor } from "./IPropertyProcessor";

export abstract class AbstractPropertyProcessor<T> implements IPropertyProcessor<T> {

    constructor() {}

    /**
     * Process item property
     * @param rProperty : item property
     * @param rProcessorCfg : processor config
     */
    abstract process(rProperty :GetPagePropertyResponse, rProcessorCfg: T): GetPagePropertyResponse;
}
