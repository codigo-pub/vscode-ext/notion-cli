import { GetPagePropertyResponse } from "@notionhq/client/build/src/api-endpoints";

export interface IPropertyProcessor<T> {
    /**
     * Process item property
     * @param rProperty : item property
     * @param rProcessorCfg : processor config
     */
    process(rProperty :GetPagePropertyResponse, rProcessorCfg: T): GetPagePropertyResponse;
}
