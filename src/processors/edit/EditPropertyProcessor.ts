import { GetPagePropertyResponse } from "@notionhq/client/build/src/api-endpoints";
import { AbstractPropertyProcessor } from "../AbstractPropertyProcessor";
import { DbEditRule } from "../../models/DbEditRule";
import { DbRule } from "../../models/DbRule";
import { ObjectTools } from "../../tools/ObjectTools";

export class EditPropertyProcessor extends AbstractPropertyProcessor<DbEditRule> {
  process(rProperty: GetPagePropertyResponse, rEditRule: DbEditRule): GetPagePropertyResponse {
    const data = rProperty as any;
    // depending on type of data
    switch (rProperty.type) {
      case "title":
        if (
          this.checkRule({
            rTxt: (rProperty as any).title[0].text.content,
            rRule: rEditRule.rule,
          })
        ) {
          (rProperty as any).title[0].text.content = rEditRule.propertyValue;
        }
        break;
      case "rich_text":
        if (
          this.checkRule({
            rTxt: (rProperty as any).rich_text[0].text.content,
            rRule: rEditRule.rule,
          })
        ) {
          (rProperty as any).rich_text[0].text.content = rEditRule.propertyValue;
        }
        break;
      case "email":
      case "phone_number":
      case "url":
        if (
          this.checkRule({
            rTxt: (rProperty as any)[rProperty.type],
            rRule: rEditRule.rule,
          })
        ) {
          (rProperty as any)[rProperty.type] = rEditRule.propertyValue;
        }
        break;
      case "checkbox":
        if (
          this.checkBooleanRule({
            rBoolean: (rProperty as any).checkbox,
            rRule: rEditRule.rule,
          })
        ) {
          (rProperty as any).checkbox = rEditRule.propertyValue;
        }
        break;
      case "number":
        if (
          this.checkNumberRule({
            rNumber: (rProperty as any).number,
            rRule: rEditRule.rule,
          })
        ) {
          (rProperty as any).number = rEditRule.propertyValue;
        }
        break;
      case "select":
        if (
          this.checkObjectRule({
            rObject: (rProperty as any).select,
            rRule: rEditRule.rule,
          })
        ) {
          (rProperty as any).select = rEditRule.propertyValue;
        }
        break;
      case "status":
        if (
          this.checkObjectRule({
            rObject: (rProperty as any).status,
            rRule: rEditRule.rule,
          })
        ) {
          (rProperty as any).status = rEditRule.propertyValue;
        }
        break;
      case "relation":
        // Browse relations, looking for the rule check,
        // note that we check id property of the object
        for (let i = 0; i < (rProperty as any).relation.length; i++) {
          let singleRelation = (rProperty as any).relation[i];
          // Edit rule exceptionally if needed, propertyValue and rule eq/neq value
          ObjectTools.convertEditRuleUUID(rEditRule);
          if (
            // If rule is checked then we set the desired value
            this.checkRule({
              rTxt: singleRelation.id,
              rRule: rEditRule.rule,
            })
          ) {
            // Set the value, adjsuting uuid if there is no dash, we add them
            (rProperty as any).relation[i].id = rEditRule.propertyValue;
          }
        }
        break;
      case "people":
      case "multi_select":
        // Browse data, looking for the rule check
        for (let i = 0; i < (rProperty as any)[rProperty.type].length; i++) {
          const singleItem = (rProperty as any)[rProperty.type][i];
          if (
            // If rule is checked then we set the desired value
            this.checkObjectRule({
              rObject: singleItem,
              rRule: rEditRule.rule,
            })
          ) {
            (rProperty as any)[rProperty.type][i] = rEditRule.propertyValue;
          }
        }
        break;
      default:
        break;
    }
    return data as GetPagePropertyResponse;
  }

  /**
   * Check rule on string value
   * @param rTxt : string value
   * @param rRule : rule
   * @returns true if rule check has passed, false otherwise
   */
  private checkRule({ rTxt, rRule }: { rTxt?: string; rRule?: DbRule }): boolean {
    // no rule no game
    if (!rRule) {
      return false;
    }
    // check rules
    if (rRule.eq !== undefined) {
      return rTxt === rRule.eq;
    }
    if (rRule.neq !== undefined) {
      return rTxt !== rRule.neq;
    }
    if (rRule.contains !== undefined && typeof rTxt === "string") {
      return rTxt.includes(rRule.contains);
    }
    return false;
  }

  /**
   * Check rule on number value
   * @param rNumber : number value
   * @param rRule : rule
   * @returns true if rule check has passed, false otherwise
   */
  private checkNumberRule({ rNumber, rRule }: { rNumber?: number; rRule?: DbRule }): boolean {
    // no rule no game
    if (!rRule) {
      return false;
    }
    // check rules
    if (rRule.eq !== undefined) {
      return rNumber === rRule.eq;
    }
    if (rRule.neq !== undefined) {
      return rNumber !== rRule.neq;
    }
    return false;
  }

  /**
   * Check rule on boolean value
   * @param rBoolean : boolean value
   * @param rRule : rule
   * @returns true if rule check has passed, false otherwise
   */
  private checkBooleanRule({ rBoolean, rRule }: { rBoolean?: number; rRule?: DbRule }): boolean {
    // no rule no game
    if (!rRule) {
      return false;
    }
    // check rules
    if (rRule.eq !== undefined) {
      return rBoolean === rRule.eq;
    }
    if (rRule.neq !== undefined) {
      return rBoolean !== rRule.neq;
    }
    return false;
  }

  /**
   * Check rule on object value
   * @param rObject : object value
   * @param rRule : rule
   * @returns true if rule check has passed, false otherwise
   */
  private checkObjectRule({ rObject, rRule }: { rObject?: any; rRule?: DbRule }): boolean {
    // no rule no game
    if (!rRule) {
      return false;
    }
    // check rules
    if (rRule.eq !== undefined) {
      return ObjectTools.deepEqual(rObject, rRule.eq);
    }
    if (rRule.neq !== undefined) {
      return !ObjectTools.deepEqual(rObject, rRule.eq);
    }
    return false;
  }
}
