export class DateTools {
  /**
   * Convenient function to show date object as DD-MM-YYYY
   * @param rDate : date object
   * @returns string date formatted as DD-MM-YYYY
   */
  static getLabelDate(rDate: Date): string {
    // Extract day, month and year from date
    const jour = ("0" + rDate.getDate()).slice(-2);
    const mois = ("0" + (rDate.getMonth() + 1)).slice(-2); // Les mois commencent à 0, donc on ajoute 1
    const annee = rDate.getFullYear();
    return `${jour}-${mois}-${annee}`;
  }
}
