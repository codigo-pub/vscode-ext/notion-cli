import { DbEditRule } from "../models/DbEditRule";
import { DbRule } from "../models/DbRule";

export class ObjectTools {
  static deepEqual(rObj1: any, rObj2: any): boolean {
    // Check if both objects are of type "object"
    if (typeof rObj1 !== "object" || typeof rObj2 !== "object") {
      return rObj1 === rObj2; // Check for strict equality if not objects
    }

    // Get the keys of the objects
    const keysObject1 = Object.keys(rObj1);
    const keysObject2 = Object.keys(rObj2);

    // Check if the number of keys is the same
    if (keysObject1.length !== keysObject2.length) {
      return false;
    }

    // Check if the values of the keys are the same
    for (const key of keysObject1) {
      if (!rObj2.hasOwnProperty(key) || !this.deepEqual(rObj1[key], rObj2[key])) {
        return false;
      }
    }

    // If all checks pass, the objects are considered equal
    return true;
  }

  /**
   * Transforms a querystring id as "28accdc004c94de6b437fd31bf4fd5b2" to UUID "28accdc0-04c9-4de6-b437-fd31bf4fd5b2"
   * Notion removes the '-' in querystring so it may be useful to copy/paste as it is in the config file
   * but we should be able to put it back in the real id format (chars -> 8-4-4-4-12)
   * @param rId : data id
   */
  static transformQueryStringIdToUUID(rId: string): string {
    return rId && rId.length === 32 && rId.indexOf("-") < 0
      ? `${rId.slice(0, 8)}-${rId.slice(8, 12)}-${rId.slice(12, 16)}-${rId.slice(16, 20)}-${rId.slice(20)}`
      : rId;
  }

  /**
   * Convert text in EditRule if this is an uuid without dashes
   * @param rEditRule : EditRule
   */
  static convertEditRuleUUID(rEditRule?: DbEditRule): void {
    if (rEditRule) {
      rEditRule.propertyValue = ObjectTools.transformQueryStringIdToUUID(rEditRule.propertyValue);
      ObjectTools.convertRuleUUID(rEditRule.rule);
    }
  }

  /**
   * Convert text in rule if this is an uuid without dashes
   * We don't convert text if this is a 'contains' rule as it is not meant to store a full uuid
   * @param rRule : rule
   */
  static convertRuleUUID(rRule?: DbRule): void {
    if (rRule) {
      if (rRule.eq) { rRule.eq = ObjectTools.transformQueryStringIdToUUID(rRule.eq); }
      if (rRule.neq) { rRule.eq = ObjectTools.transformQueryStringIdToUUID(rRule.eq); }
    }
  }
}
